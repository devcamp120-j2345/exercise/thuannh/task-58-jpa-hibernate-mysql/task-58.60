package com.devcamp.jpahyberate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpahyberateApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpahyberateApplication.class, args);
	}

}
