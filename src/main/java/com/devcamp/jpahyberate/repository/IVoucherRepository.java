package com.devcamp.jpahyberate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.jpahyberate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
